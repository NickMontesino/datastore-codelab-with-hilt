package com.codelab.android.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.preferencesDataStore
import com.codelab.android.datastore.data.DataStoreManager
import com.codelab.android.datastore.data.TasksRepository
import com.codelab.android.datastore.data.UserPreferencesRepository
import com.codelab.android.datastore.ui.TasksActivity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.prefs.Preferences
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDataStore(@ApplicationContext context: Context): DataStore<androidx.datastore.preferences.core.Preferences> =
        DataStoreManager(context).userPreferencesStore

    @Provides
    @Singleton
    fun provideTaskRepository(): TasksRepository {
        return TasksRepository
    }

    @Provides
    @Singleton
    fun provideUserPreferencesRepository(store: DataStore<androidx.datastore.preferences.core.Preferences>): UserPreferencesRepository =
        UserPreferencesRepository(store)

}